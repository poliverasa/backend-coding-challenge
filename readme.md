# Innovamat coding challenge

Com a part del MVP de l’aplicació d’Innovamat, es necessita desenvolupar una funcionalitat que sigui capaç de proporcionar 
als alumnes un seguit d’activitats d’una determinada àrea per tal que puguin realitzar l’aprenentatge corresponent. 

Les activitats s'agrupen en itineraris i estan ordenades per dificultat o nivell dins de cadascun. 
La dificultat (D) és un nombre natural que va de l’1 al 10.
Un itinerari pot incloure múltiples activitats d’un mateix nivell. 
Les activitats estan ordenades absolutament dins l'itinerari (O) de tal manera que, 
donades dues activitats de dificultat D*i* i D*j* respectivament, tal que D*i* < D*j*, 
sempre es complirà que O*i* < O*j*, sent O l'ordre de l'activitat.
Dins d'un itinerari les activitats no es poden repetir. 

Les activitats es caracteritzen pel seu nom i per un identificador. 
Un activitat pot incloure diversos exercicis. 
Per poder  fer l’avaluació del resultat, les activitats incorporen  la solució de cadascun dels exercicis 
juntament amb una estimació del temps total que s’hauria de dedicar a resoldre-la.

**El funcionament de l’aplicació és el següent:** 
(Assumint que hi ha un conjunt d’activitats definides per l’itinerari, amb una activitat com a mínim de cada nivell de dificultat.)

L’estudiant, un cop identificat a l’aplicació, demana una activitat a l’API. En cas que no hagi començat l’itinerari, 
l’API retorna la primera activitat de l’itinerari. En cas que l’estudiant ja hagi completat l’itinerari, és a dir, hagi 
resolt correctament l’última activitat programada, l’API retornarà una resposta específica indicant que no hi ha més 
activitats disponibles ja que l’itinerari ja ha estat completat.

Un cop l’estudiant rep la següent activitat a realitzar, mitjançant l’aplicació, durà a terme tots els exercicis 
requerits. En acabar l’aplicació enviarà el resultat a l’API, especificant els següents paràmetres:

* Identificador de l’activitat obtinguda al pas anterior
* Identificador de l’estudiant que ha realitzat l’activitat
* Una cadena de text amb la representació ordenada del resultat dels diferents exercicis realitzats. Per exemple, per una activitat amb 4 exercicis: 
```"1_34_-5_'cap'"```. 
* El temps que ha trigat l’estudiant a fer l’activitat

Quan rebi la petició de completar activitat, l’API la processarà i retornarà el corresponent resultat per indicar si 
s’ha registrat correctament o no. Per tal de considerar una activitat completada correctament, s’ha de proporcionar les 
respostes de tots els exercicis de l’activitat.

Quan es completi una activitat, l’API calcularà quina és la propera activitat que hauria de proporcionar a l’estudiant per 
quan l’aplicació demani la següent activitat a realitzar. Per fer-ho es tindrà en compte el resultat obtingut en la 
darrera activitat i la següent taula:

| Puntuació (score) | Acció             |
|:------------------| :-----------------|
| score < 75%             | Repetir activitat |
| 75% <= score            | Següent activitat |

En el cas que l’activitat realitzada sigui la darrera de l’itinerari i s'hagi superat satisfactòriament, no es durà a terme el càlcul.

El resultat es calcula comparant la resposta proporcionada amb la solució de l’activitat.

_Bonus:_ Addicionalment, i per tal de proporcionar el factor adaptatiu a l’itinerari, es realitzarà un càlcul 
complementari per veure si l’alumne pot avançar de nivell. Aquest segon càlcul tindrà en compte el temps dedicat a 
l’activitat i la puntuació.

| Condició             |Acció             |
| :------------------| :-----------------|
| score > 75% & temps < 50% del temps estimat | Avançar un nivell |
| score > 75% & NOT(temps < 50% del temps estimat) | Mantenir nivell  |
| score < 20% & salt de nivell previ | Retrocedir un nivell (i tornar a l'activitat següent a l'última superada)|

## Exemple:

### Itinerari:

| Activitat | Ordre | Nivell    | Temps | Solució           |
|-----------| ---| ---   | ----- | -------           |
A1          | 1  | 1          | 120   | "1_0_2" |
A2          | 2  | 1          | 60    | "-2_40_56" |
A3          | 3  | 1          | 120   | "1_0" |
A4          | 4  | 1          | 180   | "1_0_2_-5_9" |
A5          | 5  | 2          | 120   | "1_0_2" |
A6          | 6  | 2          | 120   | "1_0_2" |
A7          | 7  | 3          | 120   | "1_-1_'Si'\_34\_-6" |
A8          | 8  | 3          | 120   | "1_2" |
A9          | 9  | 4          | 120   | "1_0_2" |
A10         | 10  | 5          | 120   | "1_0_2" |
A11         | 11 | 6          | 120   | "1_0_2" |
A12         | 12  | 7          | 120   | "1_0_2" |
A13         | 13 | 8          | 120   | "1_0_2" |
A14         | 14  | 9          | 120   | "1_0_2" |
A15         | 15 | 10         | 120   | "1_0_2"          |


### Seqüència:
0. Següent activitat: A1
1. A1 + 90s + "1_0_2" -> Score= 100% -> Següent activitat: A2
2. A2 + 15s + "-2_40_56" -> Score= 100% -> Següent activitat: A5
3. A5 + 180s + "0_2_1" -> Score= 0% -> Següent activitat: A3
4. A3 + 100s + "1_1" -> Score= 50% -> Següent activitat: A3
5. A3 + 80s + "1_0" ->  Score= 100% ->Següent activitat: A4
6. A4 + 100s + "1_0_2_-4_9" -> Score= 80% -> Següent activitat: A5
7. ...
8. A15 + 145s + "1_0_2" -> Score= 100% -> Següent activitat: ~


## Es demana:

1. Implementar els casos d’ús necessaris per poder gestionar els itineraris (assumint que només n'hi ha un): 
    * afegir activitat
    * llistar activitats.
2. Implementar el cas d’ús per tal d’obtenir la següent activitat que li pertoca a cada alumne. 
Per simplificar, assumim que només hi ha un alumne registrat. 
Aquest cas d’ús es pot dividir en dos: primer calcular la següent activitat en base a la puntuació i després afegir-hi 
el salts de nivell.

3. Implementar el cas d’ús que permeti registrar el resultat d’una determinada activitat.
4. (Opcional) Implementar el cas d’ús que proporcioni el progrés actual de cada alumne (llista d'activitats realitzades i puntuació) . 

### Consideracions

* Els casos d'ús han d'estar disponibles a través d'una API.
* Cal proporcionar instruccions sobre com cal arrencar i utilitzar l'aplicació.

* Per simplificar, es pot assumir que:
    * Només hi ha un itinerari definit.
    * Només hi ha un usuari al sistema
   

